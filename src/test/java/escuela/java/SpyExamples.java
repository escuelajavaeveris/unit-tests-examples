package escuela.java;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by hgranjal on 12/06/2018.
 */
public class SpyExamples {

    @Mock
    List<String> listMock = mock(ArrayList.class);

    List<String> listSpyReal = new ArrayList<>();
    @Spy
    List<String> listSpy = spy(listSpyReal);

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMockReturnsZero() throws Exception {
        String s = "dobie";
        listMock.add(new String(s));
        verify(listMock).add(s);
        assertEquals(0, listMock.size());
    }

    @Test
    public void testSpyReturnsRealValues() throws Exception {
        String s = "dobie";
        listSpy.add(new String(s));
        verify(listSpy).add(s);
        assertEquals(1, listSpy.size());
    }
}
